[[_TOC_]]

# Overview
This repository is based on the implementation of a web ui written within S. Huser's bachelor thesis [1].
The original web UI has been adapted for Go1 campus robot. The repository contains a basic implementation for 
interacting with ROS via a web UI / browser.

## Web UI structure
The webpage basically consists of a page index.html and .js files (vue.js framework was used for the implementation).
## Dataflow
The user opens the web ui with a browser on a tablet, laptop or smartphone. The web UI is hosted by an http server 
running on Go1 robot. The web ui connects to ROS via the rosbridge_websocket which is provided by 
the [rosbridge server](http://wiki.ros.org/rosbridge_server). ROS can route topics within it's distributed setup to 
other robot's control boards, e.g. to head board for accessing the head speakers.

![webpage websocket](Webui_setup.jpg)

# Prerequisites / Installation
- The http server is part of standard python3 Installation
- Rosbridge Suite can be installed with ```sudo apt-get install ros-noetic-rosbridge-server```

# Usage: WebUI for ROS Messages
To interact with Go1 ROS system via website, you need to
1. start the server as described in section [start a server using your IP address](#start-a-server-using-your-ip-address)
1. wait for Go1 ROS to be ready (topics and nodes available, 2D lidar LED has turned from yellow to green)
1. start a websocket as described in section [start websocket](#start-websocket)

After server and websocket are started messages can be exchanged between Go1 ROS and webpage. 

## Start a server using your IP address
You need to specify the IP address as the host parameter when starting the server.
The following command will start the server on your IP address and bind it to that address, so that it only listens for incoming connections on that IP. You can also specify a port number by adding it to the end of the command:

`python3 -m http.server --bind <ip address of device, e.g. Go1 nx board IP if using a wifi device for internet connection> <port>`

e.g.

`python3 -m http.server --bind 192.168.8.116 8000`

## Start websocket
To start a websocket via the **default socket at port 9090** the rosbridge_server must be started with the following command:
`roslaunch rosbridge_server rosbridge_websocket.launch`

## Go1 webpage
To use the Go1 web GUI, 
1. connect your device (laptop or mobile phone) to the same wifi network as Go1 is connected to (e.g. `HUAWEI_1F5T`)
1. start a browser and type address `http://<Go1 IP address:server-port>`, e.g. `http://192.168.8.116:8000/` to the URI / address field. This address provides access to index.html of Go1 Web UI.
1. connect to the websocket by providing Go1 IP address (and use **port :9090** this time). Click to button **Connect** afterwards. The button changes color and text to indicate that connection was established.
1. you can now click other buttons to publish messages on **various topics** to Go1 ROS system. The web UI translates clicks to ROS messages. The Go1 web UI setup supports bidirectional communication on several topics. 
Check implementation (mainly file ```main.js```) for details.

![webpage websocket](webui_draft.jpg)

## ROS counterparts
Make sure to use corresponding versions of web UI and Go1 HSLU ROS packages. The ROS topics that are published by the webui 
are handled by ROS nodes. As topics and handling must match changes on the UI can result in changes 
on node implementation and only matching versions of UI and nodes will work fine.

Relevant gitlab projects are:
- bridge_ui: web UI
- control_states: smach state machine of Go1, handles location and other requests from web UI
- guiding: package to handle location requests from web UI
- audio_sdk: package to handle audio output requests from the web UI


## Go1 specifics
ROS bridge is installed and runs on nx board due to packages availability. Server and bridge must run on the same board.  
This is why the website has been setup to run on nx board and to be accessible via wifi device network (vs. Go1 own wifi).

# Bibliography
[1] S. Huser, Roboterdaten auf dem S-bot visualisieren und Befehlsausführung mit WebUI, June 2023
