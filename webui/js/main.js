let vueApp = new Vue({
    el: "#vueApp",
    data: {
        // ros connection
        ros: null,
        rosbridge_websocket: 'ws://192.168.8.116:9090',
        connected: false, // set to true for local gui testing...
        //topic content
        msg: 'e.g. D218',
        request: '',
        empty_msg: '',
        new_msg: 'no message received yet',
        messages: []
    },
    methods: {
        connect: function() {
            // define ROSBridge connection object
            this.ros = new ROSLIB.Ros({
                url: this.rosbridge_websocket
            })
            // define callbacks
            this.ros.on('connection', () => {
                this.connected = true
                console.log('Connection to ROSBridge established!')
            })
            // define callbacks
            this.ros.on('connection', () => {
                this.connected = true
                console.log('Starting to receive Messages!')
                let topic = new ROSLIB.Topic({
                    ros: this.ros,
                    name: '/ui_request',
                    messageType: 'std_msgs/String'
                })
                // subscribe to new messages
                topic.subscribe((message) => {
                    console.log("subscribing")
                    this.messages.push(message)
                })
            })
            this.ros.on('error', (error) => {
                console.log('Something went wrong when trying to connect')
                console.log(error)
            })
            this.ros.on('close', () => {
                this.connected = false
                console.log('Connection to ROSBridge was closed!')
            })

        },
        disconnect: function() {
            this.ros.close()
        },
        // Create a ROSLIB.Topic object
        publishLocation: function(loc) {
            let publisher = new ROSLIB.Topic({
                ros: this.ros,
                name: '/ui_request',
                messageType: 'std_msgs/String'
            })
            // Create a message to publish
            if (loc == 'library') {
                this.request = 'library'
            } else if (loc == 'canteen') {
                this.request = 'canteen'
            } else if (loc == 'wing2') {
                this.request = 'wing2'
            } else if (loc == 'wing5') {
                this.request = 'wing5'
            } else if (loc == 'home') {
                this.request = 'home'
            } else if (loc == 'test') {
                this.request = 'test'
            } else if (loc == 'halt') {
                this.request = 'halt'
            } else {
                this.request = 'unknown location'
            }
            let message = new ROSLIB.Message({
                data : this.request
            })
            console.log('Publishing message: '+ message.data)
            // Publish the message on the topic
            publisher.publish(message)
        },
        // Create a ROSLIB.Topic object
        publishFreeLocation: function() {
            let publisher = new ROSLIB.Topic({
                ros: this.ros,
                name: '/ui_request',
                messageType: 'std_msgs/String'
            })
            // Create a message to publish
            let message = new ROSLIB.Message({
                data : this.msg
            })
            console.log('Publishing message: '+ message.data)
            // Publish the message on the topic
            publisher.publish(message)
        },
        publishDemo: function() {
            let publisher = new ROSLIB.Topic({
                ros: this.ros,
                name: '/ui_request',
                messageType: 'std_msgs/String'
            })
            // Create a message to publish
            this.request = 'demo'
            let message = new ROSLIB.Message({
                data : this.request
            })
            console.log('Publishing message: '+ message.data)
            // Publish the message on the topic
            publisher.publish(message)
        },
        publishWaterBottle: function() {
            let publisher = new ROSLIB.Topic({
                ros: this.ros,
                name: '/ui_request',
                messageType: 'std_msgs/String'
            })
            // Create a message to publish
            this.request = 'bottle'
            let message = new ROSLIB.Message({
                data : this.request
            })
            console.log('Publishing message: '+ message.data)
            // Publish the message on the topic
            publisher.publish(message)
        },
    },
    mounted() {
        // page is ready
        console.log('page is ready!')
    },
})
